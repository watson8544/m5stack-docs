# 产品列表

中文 | [English](en/product_documents.md) | [日本語](ja/product_documents_ja.md)

<img src='assets/img/product_pics/1.jpg'> <img src='assets/img/product_pics/cores.png'>

| M5Core        | MiniCore      |
| :----------:  |:------------: |
| [BASIC](product_documents/m5stack-core/m5core_basic)         | [Stick](minicore_stick)         |
| [GRAY](product_documents/m5stack-core/m5core_gray)          | /            |
| [FIRE](product_documents/m5stack-core/m5core_fire)          | /            |



<img src='assets/img/product_pics/2.jpg'> <img src='assets/img/product_pics/module.png'>

| 无线通信模块      | 配件模块  | 控制模块   |
| :------------------:  |:------------------:| :--------------------:|
| [GPS](product_documents/modules/module_gps) | [PROTO](product_documents/modules/module_proto) | [STEPMOTOR](product_documents/modules/module_stepmotor)|
| [LORA](product_documents/modules/module_lora)                  | [BATTERY](product_documents/modules/module_battery)            | [PLC](product_documents/modules/module_plc)                     |
| [LAN](product_documents/modules/module_lan)                   | [BTC](product_documents/modules/module_btc)                | [FACES](product_documents/modules/module_face)                     |
| [SIM800/GPRS/GSM](product_documents/modules/module_sim800)       | [PLUS](product_documents/modules/module_plus)                  | /                     |
| /                     | /                  | /                     |
| /                     | /                  | /                     |
| /                     | /                  | /                     |
| /                     | /                  | /                     |
| /                     | /                  | /                     |


<img src='assets/img/product_pics/5.jpg'> <img src='assets/img/product_pics/accessory.png'>

- [LEGO-CABLE](product_documents/cables/accessory_lego_cable)
- [FRAME](product_documents/accessory_frame)



<img src='assets/img/product_pics/3.jpg'> <img src='assets/img/product_pics/unit.png'>

| 输入/传感类Unit   | 输出/控制类Unit  | 接口类Unit   |
| :-------------------: |:------------------------: | :----------------:|
| [ENV](product_documents/units/unit_env)                   | [RGB](product_documents/units/unit_rgb)                       | [HUB](product_documents/units/unit_hub)               |
| [IR](product_documents/units/unit_ir)                    | [RELAY](product_documents/units/unit_relay)                         | [3.96PORT](product_documents/units/unit_396port)          |
| [PIR](product_documents/units/unit_pir)                   | [NeoPixel](product_documents/units/unit_neopixel)                         | /                 |
| [ANGLE](product_documents/units/unit_angle)                   | /                         | /                 |
| [EARTH/Moisture](product_documents/units/unit_moisture)        | /                         | /                 |
| [LIGHT](product_documents/units/unit_light)                 | /                         | /                 |
| [MAKEY](product_documents/units/unit_makey)                   | /                         | /                 |
| [BUTTON](product_documents/units/unit_button)                   | /                         | /                 |
| [Dual-BUTTON](product_documents/units/unit_dual_button)                   | /                         | /                 |
| [JOYSTICK](product_documents/units/unit_joystick)                   | /                         | /                 |
| [THERMAL](product_documents/units/unit_thermal)                   | /                         | /                 |
| [ADC](product_documents/units/unit_ADC)                   | /                         | /                 |
| [DAC](product_documents/units/unit_DAC)                   | /                         | /                 |
| [Color Sensor](product_documents/units/unit_color_sensor)                   | /                         | /                 |
| [ToF](product_documents/units/unit_tof)                   | /                         | /                 |





<img src='assets/img/product_pics/4.jpg'> <img src='assets/img/product_pics/application.png'>

- [BALA](product_documents/applications/application_bala)


<img src='assets/img/product_pics/6.jpg'> <img src='assets/img/product_pics/tool.png'>

- [M5Stack USB Downloader](product_documents/tools/tool_usb_downloader)
