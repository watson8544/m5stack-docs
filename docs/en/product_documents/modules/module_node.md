# Node Module

## DESCRIPTION

The Node is a sound box module with 12 RGBLed(SK6812), a codec chip(WM8978), DHT12, IR Transmitter and Receiver(CHQ0038H), two MICs. WM8978 is often used to be applied for Hi-Fi Speaker.
You can program it after connected to any series of M5Stack Core through Arduino.

You can creat a Webradio, Bluetooth Speaker, even Intelligent sound box with this module.

## FEATURES

-  Including 12 RGBLed
-  Including a HiFi stereo codec chip(Up to 24bit DAC)
-  Including a lithium battery interface

## INCLUDES

-  1x Node Module

## Applications

-  Webradio
-  Bluetooth Speaker
-  Intelligent sound box

## DOCUMENTS

- [WebSite](https://m5stack.com)
- [WM8978](http://pdf1.alldatasheet.com/datasheet-pdf/view/96647/WOLFSON/WM8978.html) (WM8978)
- [Schematic]()
- [Example](https://github.com/m5stack/M5Stack/tree/master/examples/Modules/Lora)
    - [RGB Example](https://github.com/Makuna/NeoPixelBus/tree/master/examples)
- [Quick Start]()
- [Purchase]()
