# M5Stack BASIC

## DESCRIPTION

The M5Stack **<mark>BASIC</mark>** is a development kit based on <mark>ESP32</mark> chip. You can even
program The M5Stack BASIC through Blockly, Arduino or MicroPython.

The M5Stack BASIC equips the ESP32 with everything necessary to program,
and a TFT LCD, so you can create a 3D remote gesture controller, a
simple "Leap Motion" via M5Stack BASIC in a day in stead of couple weeks
and so on.

## FEATURES

-  Programming Support
-  Arduino
-  ESP-IDF
-  MicroPython
-  TF Card Support

## PARAMETER

| Source        | Parameter      |
| :----------:  |:------------: |
| <mark>ESP32</mark>         | 240MHz dual core, 600 DMIPS, 520K, Wi-Fi, dual mode Bluetooth         |
| Flash)          | 4M-Bytes            |
| Input          | 5V @ 500mA            |
| Interface          | TypeC x 1, GROVE(I2C+I/0+UART) x 1            |
| LCD          | 2 inch, 320x240 Colorful TFT LCD, ILI9342            |
| Speaker          | 1W-0928            |
| Battery          | 150mAh @ 3.7V, inside  vb            |
| Op.Temp.          | 32°F to 104°F ( 0°C to 40°C )            |
| Size          | 54 x 54 x 12.5 mm            |
| C.A.S.E          | Plastic ( PC )            |
| Weight          | 120g with bottom, 100g only core            |

## INCLUDES

-  1x M5Stack BASIC
-  1x M5Stack BASIC Bottom
-  Type-C USB Cable
-  User Manual

## DOCUMENTS

-  **[Schematic](https://github.com/m5stack/M5-3D_and_PCB/blob/master/M5_Core_SCH%2820171206%29.pdf)**

-  **Example** - [Arduino Example](https://github.com/m5stack/M5Stack/tree/master/examples)

-  **Datasheet** - [ESP32](https://www.espressif.com/sites/default/files/documentation/esp32_datasheet_cn.pdf)

-  **GitHub** - [Arduino GitHub](https://github.com/m5stack/M5Stack)

-  **<mark>Quick Start</mark>** - Arduino - [MacOS](../../quick_start/m5core/m5stack_core_get_started_Arduino_MacOS) - [Windows_64](../../quick_start/m5core/m5stack_core_get_started_Arduino_Windows) - [MicroPython](../../quick_start/m5core/m5stack_core_get_started_MicroPython)
