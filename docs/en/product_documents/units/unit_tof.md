# Unit ToF

## DESCRIPTION

This is a unit can detect distance using a newest "Time-to-Flight" sensor using laser light. It is higher precision than most distance sensors. The unit comunicates with M5Core with I2C.

## FEATURES

-  High precision
-  Measure absolute distances up to 2m
-  Two Lego installation holes

## APPLICATION

-  1D gesture recognition

## DOCUMENTS

-  GitHub

   - [Arduino](https://github.com/m5stack/M5Stack)

-  Datasheet

   - [VL53L0X](https://pdf1.alldatasheet.com/datasheet-pdf/view/948120/STMICROELECTRONICS/VL53L0X.html)

-  [Schematic](https://github.com/m5stack/M5Stack)

-  [Purchase](https://www.aliexpress.com/store/product/M5Stack-Official-ToF-Unit-VL53L0X-Time-of-Flight-ToF-Laser-Ranging-Sensor-Breakout-Laser-Distance-Sensor/3226069_32949310300.html?spm=a2g1x.12024536.productList_5885013.pic_3)

<figure>
    <img src="assets/img/product_pics/units/M5GO_Unit_tof.png" height="300" width="300">
</figure>
