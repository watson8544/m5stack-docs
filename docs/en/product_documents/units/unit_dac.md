# Unit ADC

## DESCRIPTION

This is a unit can convert digital signal to analog signal like voltage waveform, audio waveform and so on. It integrates a 12-bit high resolution DAC chip named MCP4725 which integrates a on-board non-volatile memory (EEPROM). The unit comunicates with M5Core with I2C. The DAC input and configuration data can be programmed to the EEPROM.

## FEATURES

-  Up to 12 bits of resolution
-  Output 0~3.3V voltage
-  Two Lego installation holes

## APPLICATION

-  MP3 Audio Player
-  mini Oscilloscope

## DOCUMENTS

-  GitHub

   - [Arduino](https://github.com/m5stack/M5Stack)

-  Datasheet

   - [MCP4725](http://pdf1.alldatasheet.com/datasheet-pdf/view/233449/MICROCHIP/MCP4725.html)

-  [Purchase](https://www.aliexpress.com/store/product/M5Stack-Official-DAC-Unit-MCP4725-I2C-DAC-Converter-Breakout-Module-Digital-to-Analog-12-Bits-0V/3226069_32947696641.html?spm=a2g1x.12024536.productList_5885013.pic_6)

<figure>
    <img src="assets/img/product_pics/units/M5GO_Unit_dac.png" height="300" width="300">
</figure>
