# Unit NeoPixel

## DESCRIPTION

This is a RGB LED Cable unit. You can program it using NeoPixel Lib supported by Adafruit or using M5Flow that will be easier. The unit comunicates with M5Core with GROVE Interface.

## FEATURES

-  the length: 10cm/20cm/0.5m/1m/2m
-  GROVE Interface
-  Two Lego installation holes

## DOCUMENTS

-  GitHub

   - [Arduino](https://github.com)

-  [Purchase](https://www.aliexpress.com/store/product/M5Stack-Official-NeoPixel-RGB-LEDs-Cable-SK6812-with-GROVE-Port-2m-1m-50cm-20cm-10cm/3226069_32950831315.html?spm=a2g1x.12024536.productList_5885013.pic_0)

<figure>
    <img src="assets/img/product_pics/units/M5GO_Unit_neopixel.png" height="300" width="300">
</figure>
