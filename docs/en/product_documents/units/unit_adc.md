# Unit ADC

## DESCRIPTION

This is a unit having self-calibrating function and 16bit analog-to-digitial coverter. The resolution is double than insided ADC of esp32, that means you can detect smaller amplitude voltage. The unit communicates with M5Core with I2C. It has provided two modes: continuously conversion and single conversion.

## FEATURES

-  Up to 16 bits of resolution and perform conversions at rates of 8, 16, 32, or 128 samples per second
-  Programmable gain amplifier
    - Gain = 1, 2, 4, OR 8
-  Detect 0~12V voltage Input
-  Two Lego installation holes

## APPLICATION

-  ECG signal acquisition
-  Blood pressure measurement
-  Dynamometer

## DOCUMENTS

-  GitHub

   - [Arduino](https://github.com/m5stack/M5Stack)

-  Datasheet

   - [ADS1100](http://pdf1.alldatasheet.com/datasheet-pdf/view/619024/TI1/ADS1100.html)

-  [Purchase](https://www.aliexpress.com/store/product/M5Stack-Official-ADC-Unit-16-Bit-I2C-GROVE-ADS1100-Module-0V-to-12V-analog-to-digital/3226069_32946953374.html?spm=a2g1x.12024536.productList_5885013.pic_7)

<figure>
    <img src="assets/img/product_pics/units/M5GO_Unit_adc.png" height="300" width="300">
</figure>